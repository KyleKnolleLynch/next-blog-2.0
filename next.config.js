/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'raw.githubusercontent.com',
        port: '',
        pathname: '/KyleKnolleLynch/my-blog-2.0-blogposts/main/images/**'
      }
    ]
  }
}

module.exports = nextConfig
