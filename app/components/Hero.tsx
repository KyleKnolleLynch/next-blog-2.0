import Image from 'next/image'

export default function Hero() {
  return (  
    <section className='w-full h-96 md:h-144 mx-auto relative isolate'>
      <Image
        src='/images/hero_bg.avif'
        alt='Road with mountains in the background'
        fill
        priority={true}
        className='drop-shadow-xl shadow-black object-cover'
      />
      <span className='absolute inset-0 mix-blend-multiply bg-clr-accent'></span>
      <div className='relative pt-60 md:pt-96 pl-4 text-white'>
        <h1 className='text-5xl md:text-7xl pb-4'>My Blog</h1>
        <p className='text-xl md:text-3xl'>Welcome to my new blog</p>
      </div>
    </section>
  )
}
