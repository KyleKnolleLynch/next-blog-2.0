# My blog version 2.0

* This is a rebuild of my original nextjs blog using Nextjs 13. This version uses local markdown files for blog article content, rather than pull from Contentful CMS. Styled with TailwindCSS now, rather than styled jsx in my previous blog. 

I want to give a big thanks to [Dave Gray]('https://www.youtube.com/DaveGrayTeachesCode') for his tutorial, helping me understand how to use local markdown files with Next version 13. I used his base code and modified it to give it my own look and feel.