'use client'
import { useEffect, useState } from 'react';
import Link from 'next/link'
import { useRouter } from "next/navigation";
import { FiHome } from 'react-icons/fi'


export default function ContactSuccess() {
  const [countDown, setCountDown] = useState(10)
  const router = useRouter()

  useEffect(() => {
    const timer = setInterval(() => {
      setCountDown(prev => prev - 1)
    }, 1000)

    countDown === 0 && router.push('/')

    return () => clearInterval(timer)
  }, [countDown, router])

  return (
    <main className='min-h-screen bg-clr-accent p-4'>
      <section className='max-w-2xl mx-auto mt-16 p-4 text-center rounded-md bg-zinc-100 text-clr-accent'>
        <h1 className='text-3xl md:text-6xl mt-6 md:mt-12'>Success!</h1>
        <p className='mt-6 text-lg md:text-xl'>
          Your info has been submitted successfully. I will get back to you as
          soon as possible. Thanks for the feedback!
        </p>
        <p className='mt-6 text-lg md:text-xl flex items-center justify-center'>
          Head back{' '}
          <Link
            href='/'
            className='flex items-center gap-1 pl-3 hover:underline'
          >
            <FiHome />
            Home
          </Link>
        </p>
        <small className='py-3 block'>{`(Redirecting you in ${countDown} seconds)`}</small>
      </section>
    </main>
  )
}
