import Hero from './components/Hero'
import Navbar from './components/Navbar'
import Sidebar from './components/Sidebar'
import Posts from './components/Posts'
import Footer from './components/Footer'

export const revalidate = 86400


export default function Home() {
  return (
    <div className='relative'>
      <Navbar />
      <main className='max-w-screen-2xl mx-auto pb-16'>
        <Hero />
        <div className='max-w-5xl grid grid-cols-1 md:grid-cols-layout gap-4 justify-center mt-6 mx-auto px-4'>
          <Sidebar />
          {/* @ts-expect-error Server Component */}
          <Posts />
        </div>
      </main>
      <Footer textColor='text-zinc-900' darkModeTextColor='text-zinc-50' />
    </div>
  )
}
