import { useState, useEffect } from 'react'
import { useTheme } from 'next-themes'
import { IoBulbOutline, IoBulbSharp } from 'react-icons/io5'

export default function ThemeSwitch() {
  const [mounted, setMounted] = useState(false)
  const { theme, systemTheme, setTheme } = useTheme()

  useEffect(() => {
    setMounted(true)
  }, [])

  if (!mounted) return null

  const currentTheme = theme === 'system' ? systemTheme : theme

  return (
    <>
      {currentTheme === 'dark' ? (
        <button
          onClick={() => setTheme('light')}
          className='mr-8 p-2 border border-zinc-50/80 text-zinc-50/80 hover:border-zinc-50 hover:text-zinc-50 rounded-lg'
          aria-label='Toggle light theme'
        >
          <IoBulbSharp />
        </button>
      ) : (
        <button
          onClick={() => setTheme('dark')}
          className='mr-8 p-2 border  border-clr-accent-light text-clr-accent-light hover:brightness-90 rounded-lg'
          aria-label='Toggle dark theme'
        >
          <IoBulbOutline />
        </button>
      )}
    </>
  )
}
