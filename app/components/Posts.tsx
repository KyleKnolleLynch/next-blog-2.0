import { getPostsMeta } from '@/lib/posts'
import ListItem from './ListItem'

export default async function Posts() {
  const posts = await getPostsMeta()

  if (!posts) {
    return <p className='mt-10 text-center'>Sorry, no posts available.</p>
  }
 
  return (
    <section className='max-w-2xl mx-auto mt-6 px-2'>
      <h2 className='text-4xl font-bold dark:text-zinc-50 text-center mb-10'>
        Articles
      </h2>
      <ul className='w-full list-none p-0'>
        {posts
          .map(post => <ListItem key={post.id} post={post} />
          )}
      </ul>
      {/* <div className='text-center'>
        <button
          className='w-1/2 mt-12 py-1 border border-black/60 text-black/60 hover:border-black hover:text-black dark:text-zinc-50/60 dark:border-zinc-50/60 hover:dark:text-zinc-50 hover:dark:border-zinc-50 rounded-lg font-bold'
        >
          Show More
        </button>
      </div> */}
    </section>
  )
}
