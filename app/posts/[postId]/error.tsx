'use client' // Error components must be Client components

import Link from 'next/link'
import { useEffect } from 'react'
import { FiHome } from 'react-icons/fi'

export default function Error({
  error,
  reset,
}: {
  error: Error
  reset: () => void
}) {
  useEffect(() => {
    // Log the error to an error reporting service
    console.error(error)
  }, [error])

  return (
    <main className='mx-auto max-w-lg py-1 px-4'>
      <h2 className='my-4 text-2xl font-bold'>Something went wrong!</h2>
      <button
        onClick={
          // Attempt to recover by trying to re-render the segment
          () => reset()
        }
        className='mb-4 p-4 bg-red-500 text-white rounded-xl'
      >
        Try again
      </button>
      <p className='md:text-2xl flex items-center justify-center'>
        Or go back
        <Link
          href='/'
          className='pl-3 font-bold flex items-center gap-1 underline hover:text-zinc-50'
        >
          <FiHome />
          Home
        </Link>
      </p>
    </main>
  )
}
