/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        'clr-accent': 'hsl(265, 90%, 45%)',
        'clr-accent-light': 'hsl(265, 90%, 60%)',
        'clr-secondary': 'hsl(265, 80%, 38%)',
        'clr-secondary-dark': 'hsl(265, 75%, 18%)',
      },
      spacing: {
        144: '36rem',
      },
      gridTemplateColumns: {
        layout: 'auto 1fr ',
      },
      fontFamily: {
        sans: ['var(--font-rubik)'],
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
