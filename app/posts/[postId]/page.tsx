import getFormattedDate from '@/lib/getFormattedDate'
import { getPostsMeta, getPostByName } from '@/lib/posts'
import Navbar from '../../components/Navbar'
import Link from 'next/link'
import { notFound } from 'next/navigation'
import { FiArrowUpLeft } from 'react-icons/fi'
import 'highlight.js/styles/tokyo-night-dark.css'

export const revalidate = 86400

type Props = {
  params: {
    postId: string
  }
}

export async function generateStaticParams() {
  const posts = await getPostsMeta() // deduped

  if (!posts) return []

  return posts.map(post => ({
    postId: post.id,
  }))
}

export async function generateMetadata({ params: { postId } }: Props) {
  const post = await getPostByName(`${postId}.mdx`) // deduped

  if (!post) {
    return {
      title: 'Post Not Found',
    }
  }

  return {
    title: post.meta.title,
  }
}

export default async function Post({ params: { postId } }: Props) {
  const post = await getPostByName(`${postId}.mdx`) // deduped

  if (!post) notFound()

  const { meta, content } = post
  const pubDate = getFormattedDate(meta.date)

  const tags = meta.tags.map((tag, i) => (
    <Link key={i} href={`/tags/${tag}`}>
      {tag}
    </Link>
  ))

  return (
    <>
      <Navbar />
      <main className='prose prose-xl prose-zinc dark:prose-invert mx-auto px-6 py-10'>
        <h1 className='text-3xl mt-6 mb-0'>{meta.title}</h1>
        <p>{pubDate}</p>
        <div>
          <article>{content}</article>
          <section>
            <h2>Related:</h2>
            <div className='flex gap-4'>{tags}</div>
          </section>
          <p>
            <Link
              href='/'
              className='inline-flex items-center gap-2 text-clr-secondary'
            >
              <span className='text-2xl'>
                <FiArrowUpLeft />
              </span>{' '}
              Back to home
            </Link>
          </p>
        </div>
      </main>
    </>
  )
}
