import Image from 'next/image'
import Link from 'next/link'

import { FiGitlab, FiGithub, FiCodepen, FiGlobe } from 'react-icons/fi'

export default function Sidebar() {
  return (
    <aside className='place-self-center grid grid-cols-1 md:grid-cols-layout gap-6 place-items-center p-2'>
      <Image
        src='/images/profile_pic.avif'
        alt='Kyle Lynch'
        width={100}
        height={100}
        priority={true}
        className='rounded-full border-2 border-black dark:border-zinc-600 drop-shadow-xl shadow-black object-cover md:w-32 md:h-32'
      />
      <div className='text-zinc-900 dark:text-zinc-50 text-3xl flex justify-center items-center gap-4 md:flex-col md:gap-2'>
        <Link
          aria-label='My portfolio website'
          href='https://kylelynch.me'
          target='_blank'
          className='p-2 rounded-full hover:bg-clr-accent/30 hover:shadow-lg dark:hover:bg-clr-accent/50 transition-all'
        >
          <FiGlobe />
        </Link>
        <Link
          aria-label='My Github profile'
          href='https://github.com/KyleKnolleLynch'
          target='_blank'
          className='p-2 rounded-full hover:bg-clr-accent/30 hover:shadow-lg dark:hover:bg-clr-accent/50 transition-all'
        >
          <FiGithub />
        </Link>
        <Link
          aria-label='My Gitlab profile'
          href='https://gitlab.com/KyleKnolleLynch'
          target='_blank'
          className='p-2 rounded-full hover:bg-clr-accent/30 hover:shadow-lg dark:hover:bg-clr-accent/50 transition-all'
        >
          <FiGitlab />
        </Link>
        <Link
          aria-label='My codepen profile'
          href='https://codepen.io/KyleKnolleLynch'
          target='_blank'
          className='p-2 rounded-full hover:bg-clr-accent/30 hover:shadow-lg dark:hover:bg-clr-accent/50 transition-all'
        >
          <FiCodepen />
        </Link>
      </div>
    </aside>
  )
}
