type Props = {
  textColor: string
  darkModeTextColor?: string
}

export default function Footer({ textColor, darkModeTextColor }: Props) {
  const year = new Date().getFullYear()

  return (
    <footer
      className={`absolute bottom-0 left-0 right-0 mx-auto p-2 text-center bg-transparent ${textColor} dark:${darkModeTextColor}`}
    >
      <p className='relative'>
        <small>
          <strong>&copy; {year} Kyle Lynch</strong>
        </small>
        <span className='absolute -top-1 left-0 right-0 w-2/3 md:w-1/2 lg:w-1/3 border-t-2 border-zinc-200/50 mx-auto'></span>
      </p>
    </footer>
  )
}
