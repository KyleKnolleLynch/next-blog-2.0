import { Rubik } from 'next/font/google'
import './globals.css'
import { Providers } from './components/Providers'

const rubik = Rubik({
  variable: '--font-rubik',
  subsets: ['latin'],
  display: 'swap',
})

export const metadata = {
  title: "Kyle Lynch's blog version 2",
  description: 'Created by Kyle Lynch',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang='en' suppressHydrationWarning className={`${rubik.variable}`}>
      <body className='dark:bg-zinc-800 bg-zinc-50 leading-relaxed'>
        <Providers>{children}</Providers>
      </body>
    </html>
  )
}
