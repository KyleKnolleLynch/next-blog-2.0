import Link from 'next/link'
import { FiHome } from 'react-icons/fi'

export default function NotFound() {
  return (
    <main className='h-screen bg-clr-accent text-zinc-100 text-center'>
      <h1 className='pt-20 text-5xl md:text-7xl'>Oops!</h1>
      <h2 className='pt-6 text-xl md:text-3xl'>That post does not exist.</h2>

      <p className='pt-6 md:text-2xl flex items-center justify-center'>
        Let&apos;s go back
        <Link
          href='/'
          className='pl-3 font-bold flex items-center gap-1 underline hover:text-zinc-50'
        >
          <FiHome />
          Home
        </Link>
      </p>
    </main>
  )
}
