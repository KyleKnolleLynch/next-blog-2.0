'use client'
import axios from 'axios'
import { useForm, SubmitHandler } from 'react-hook-form'
import { useRouter } from 'next/navigation'

import Navbar from '../components/Navbar'
import Footer from '../components/Footer'

type Inputs = {
  name: string
  email: string
  message: string
}

export default function Contact() {
  const router = useRouter()
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<Inputs>()
  const onSubmit: SubmitHandler<Inputs> = data => {
    axios
      .post(
        `https://${process.env.NEXT_PUBLIC_PIPEDREAM_TRIGGER_API}.m.pipedream.net`,
        data
      )
      .then(response => {
        router.push('/contactSuccess')
      })
      .catch(e => console.error(e))
  }

  return (
    <div className='min-h-screen  bg-clr-accent'>
      <Navbar />
      <main>
        <h1 className='text-3xl md:text-5xl pt-6 md:pt-12 text-center text-zinc-50'>
          Contact Me
        </h1>
        <section className='max-w-2xl mx-auto py-6 md:py-12 px-2'>
          <form
            action='/contactSuccess'
            className='p-4 rounded-md bg-zinc-100 text-zinc-900 dark:bg-zinc-900 dark:text-zinc-50'
            onSubmit={handleSubmit(onSubmit)}
          >
            <label className='text-lg md:text-xl font-bold'>
              Name
              <small
                className={`text-red-700 ml-2 p-1 ${
                  errors.name ? 'opacity-100' : 'opacity-0'
                }`}
              >
                This field is required
              </small>
              <input
                type='text'
                placeholder='Enter your name'
                {...register('name', { required: true })}
                className='w-full block mb-6 p-2 font-normal rounded-md dark:bg-zinc-600 dark:placeholder:text-zinc-50'
              />
            </label>

            <label className='text-lg md:text-xl font-bold'>
              Email
              <small
                className={`text-red-700 ml-2 p-1 ${
                  errors.email ? 'opacity-100' : 'opacity-0'
                }`}
              >
                This field is required
              </small>
              <input
                type='email'
                placeholder='Enter your email'
                {...register('email', { required: true })}
                className='w-full block mb-6 p-2 font-normal rounded-md dark:bg-zinc-600 dark:placeholder:text-zinc-50'
              />
            </label>

            <label className='text-lg md:text-xl font-bold'>
              Message
              <small
                className={`text-red-700 ml-2 p-1 ${
                  errors.message ? 'opacity-100' : 'opacity-0'
                }`}
              >
                Message must be at least 20 characters
              </small>
              <textarea
                placeholder='Enter your message'
                {...register('message', { required: true, minLength: 20 })}
                className='w-full block mb-6 p-2 font-normal rounded-md dark:bg-zinc-600 dark:placeholder:text-zinc-50'
              ></textarea>
            </label>

            <button className='mx-auto mt-10 mb-4 block py-2 px-4 rounded-md font-bold text-lg md:text-xl text-zinc-50 bg-clr-accent hover:bg-clr-accent/80'>
              {isSubmitting ? 'Submitting' : 'Submit'}
            </button>
          </form>
        </section>
      </main>
      <Footer textColor='text-zinc-50/90' />
    </div>
  )
}
