import Image from 'next/image'

import Navbar from '../components/Navbar'
import Footer from '../components/Footer'

export default function About() {
  return (
    <div className='min-h-screen bg-clr-accent'>
      <Navbar />
      <main>
        <section className='max-w-2xl text-center mx-auto p-6 text-zinc-50'>
          <h1 className='text-5xl pt-4 md:text-7xl md:pt-10'>Who am I?</h1>
          <Image
            src='/images/profile_pic.avif'
            alt='Kyle Lynch'
            width={200}
            height={200}
            priority={true}
            className='mt-8 md:mt-16 mx-auto rounded-full border-2 border-black dark:border-zinc-600 drop-shadow-xl shadow-black object-cover'
          />
          <p className='mt-6 md:text-xl'>
            Hi, I'm Kyle, a web developer focused on the Javascript ecosystem. I
            engineered this blog as a place to discuss technology, cars, or any
            other subjects that interest me.
          </p>
        </section>
      </main>
      <Footer textColor='text-zinc-50' darkModeTextColor='text-zinc-50' />
    </div>
  )
}
